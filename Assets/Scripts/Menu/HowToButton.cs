﻿using UnityEngine;
using System.Collections;

public class HowToButton : MonoBehaviour {

    //public GoogleAnalyticsV4 googleAnalytics;//Google Analytics Plugin for Unity

    public GameObject cam;      //The menu camera
    private AudioSource sound;  //The sound effect

    void Start()
    {
        sound = GetComponent<AudioSource>();//Set the sound effect
    }

    void OnMouseDown()
    {
        if (this.name == "HowToButton")
        {
            //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Viewed Screen").SetEventAction("Viewed How To Play"));
            cam.GetComponent<MenuCamera>().HowTo();//Call the HowTo method in the main camera to face the How To Play screen
            sound.Play();//Play the sound effect
        }
    }
}
