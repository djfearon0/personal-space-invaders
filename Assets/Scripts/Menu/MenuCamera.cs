﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuCamera : MonoBehaviour {

    public GameObject highscore;//The current highscore of the game
    public GameObject menuMusic;//The music for the main menu
    public Image fade;          //The black texture for the fade
    private AudioSource music;  //The audio source for the music

    private bool credits;   //The boolean to turn toward credits
    private bool fading;    //The boolean to fade the camera
    private bool howTo;     //The boolean to turn toward How To Play
    private bool main;      //The boolean to turn toward the main menu

	// Use this for initialization
	void Start () {

        if (PlayerPrefs.HasKey("highscore"))//Check if a highscore exists
        {
            highscore.GetComponent<TextMesh>().text = "" + PlayerPrefs.GetInt("highscore");//It exists, display it on the How To Play screen
        }
        else
        {
            highscore.GetComponent<TextMesh>().text = "0";//It does not exist, show zero
        }

        fade.transform.localScale = new Vector3(Screen.width/20, Screen.height/20, 0);//Adjust the fade texture to the size of the viewport
        Color fadeColor = fade.color;   //Get the color of the fade texture
        fadeColor.a = 0;                //Make the fade transparent
        fade.color = fadeColor;         //Set the new transparency

        music = menuMusic.GetComponent<AudioSource>();//Get the music of the menu
    }
	
	// Update is called once per frame
	void Update () {

        Quaternion current = transform.rotation;//Get the current rotation of the camera

        if (current == Quaternion.Euler(0, -90, 0))//Check if the screen is facing credits, turn the boolean false
        {
            credits = false;
        } else if (current == Quaternion.Euler(0, 90, 0))//Check if the screen is facing How To Play, turn the boolean false
        {
            howTo = false;
        } else if (current == Quaternion.Euler(Vector3.zero))//Check if the screen is facing the main menu, turn the boolean false
        {
            main = false;
        }

	    if (fading)//See if the camera is fading
        {
            fade.color = Color.Lerp(fade.color, Color.black, 3 * Time.deltaTime);   //Slowly make the color less transparent until it is completely black
            music.volume -= Time.deltaTime;                                         //Decrease the volume of the music gradually
            transform.Translate(0, 0, 3 * Time.deltaTime);                          //Move the camera into the menu
        } else if (credits)//See if the credits button was clicked
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, -90, 0)), 0.3f);//Rotate to credits
        } else if (howTo)//See if the How To Play button was clicked
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, 90, 0)), 0.3f);//Rotate to How To Play
        } else if (main)//See if a back button was clicked
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(Vector3.zero), 0.3f);//Rotate back to the main menu
        }
	}

    //Set teh fade boolean to true
    public void FadeToBlack()
    {
        fading = true;
    }

    //Set the howTo boolean to true
    public void HowTo()
    {
        howTo = true;
    }

    //Set the credits boolean to true
    public void Credits()
    {
        credits = true;
    }

    //Set the main boolean to true
    public void Back()
    {
        main = true;
    }
}
