﻿using UnityEngine;
using System.Collections;

public class BackButton : MonoBehaviour {

    public GameObject cam;      //Reference the camera
    private AudioSource sound;  //Get the sound effect of the button

    void Start()
    {
        sound = GetComponent<AudioSource>();//Set the sound effect
    }

    void OnMouseDown()
    {
        if (this.name == "BackButton")
        {
            cam.GetComponent<MenuCamera>().Back();//Call the back method in the camera to face the main menu
            sound.Play();//Play the sound effect
        }
    }
}
