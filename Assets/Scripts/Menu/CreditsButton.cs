﻿using UnityEngine;
using System.Collections;

public class CreditsButton : MonoBehaviour
{

    //public GoogleAnalyticsV4 googleAnalytics;//Google Analytics Plugin for Unity

    public GameObject cam;      //The menu camera
    private AudioSource sound;  //The sound effect

    void Start()
    {
        sound = GetComponent<AudioSource>();//Set the sound effect
    }

    void OnMouseDown()
    {
        if (this.name == "CreditsButton")
        {
            //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Viewed Screen").SetEventAction("Viewed Credits"));
            cam.GetComponent<MenuCamera>().Credits();//Call the credits method in the camera to face the credits menu
            sound.Play();//Play the sound effect
        }
    }
}
