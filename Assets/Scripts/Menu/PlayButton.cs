﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayButton : MonoBehaviour {

    public GameObject cam;      //The menu camera
    public Image fade;          //A black texture for fading the menu
    private AudioSource sound;  //The sound effect

    void Start()
    {
        sound = GetComponent<AudioSource>();//Set the sound effect
    }

    void Update()
    {
        if (fade.color.a >= 0.95f)//Wait until the alpha on the fade texture is 95% or higher
        {
            SceneManager.LoadScene("Arena");//Load the game
        }
    }

	void OnMouseDown()
    {
        if (this.name == "PlayButton")
        {
            cam.GetComponent<MenuCamera>().FadeToBlack();//Call the FadeToBlack method on the main camera to start the game
            sound.Play();//Play the sound effect
        }
    }
}
