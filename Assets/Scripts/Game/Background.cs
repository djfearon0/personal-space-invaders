﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

    private PersonalSpaceInvaders game; //Reference to the game logic
    private WebCamTexture arTexture;    //Texture containing feed from device camera

    // Use this for initialization
    void Start () {
        //Set the sizes for the panel to match the viewport
        double height = 2.0 * Mathf.Tan(0.5f * Camera.main.fieldOfView * Mathf.Deg2Rad) * 10;
        double width = height * Camera.main.aspect;

        //Access the game logic
        game = Camera.main.GetComponent<PersonalSpaceInvaders>();

        //Find the background plane in the sccene and scale it to the size of the screen, rotate so it is not upside down
        GameObject plane = GameObject.Find("Plane");
        plane.transform.localScale = new Vector3((float)width, 1, (float)height)/10;
        plane.transform.Rotate(0, 180, 0);

        //Create the webcam texture
        arTexture = new WebCamTexture();

        //Apply the video feed to the background panel
        plane.GetComponent<Renderer>().material.mainTexture = arTexture;

        //Play the video feed
        arTexture.Play();
    }
	
	// Update is called once per frame
	void Update () {
        //If the game is over, stop the background and release the camera
        if (game.gameover == true)
        {
            arTexture.Stop();
        }
	}
}
