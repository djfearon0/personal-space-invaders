﻿using UnityEngine;
using System.Collections;

public class ToughEnemy : MonoBehaviour {

    //public GoogleAnalyticsV4 googleAnalytics;//Google Analytics Plugin for Unity

    public GameObject distortion;   //The visual effect when the enemy enters the play area
    public GameObject explosion;    //The visual effect that plays when the enemy is destroyed
    private AudioSource warp;       //The sound effect when the enemy enter the play area
    private Color flash;            //The color to flash when damaged
    private Color normal;           //The regular color
    private GameObject player;      //Reference to the player object

    private int health = 40;//This enemy has 40 points of health
    private int score = 100;//The number of points the player gets for destroying this enemy

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Main Camera");//Get the player object (camera)
        warp = GetComponent<AudioSource>();     //Get the sound effect

        flash = normal = GameObject.Find("default").GetComponent<Renderer>().material.color;//Set the color of the ship
        flash = Color.red;//Change flash color to red
    }

    // Update is called once per frame
    void Update () {
        transform.Translate(Mathf.Sin(Time.time) * 4f * Time.deltaTime, 0, 0);                              //Move the enemy from side to side based on a Sine wave
        transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, 0.5f * Time.deltaTime);  //Move the enemy toward the player
        transform.LookAt(new Vector3(0, -180, 0));                                                          //Have the enemy facing the player

        if (health <= 0)//Check if enemy is destroyed
        {
            StartCoroutine(Die());//Destroy this enemy
            player.GetComponent<PersonalSpaceInvaders>().GainPoints(score);//Give player points
        }
    }

    //Flash red when damaged
    IEnumerator Damaged()
    {
        GameObject.Find("default").GetComponent<Renderer>().material.color = flash;
        yield return new WaitForSeconds(0.2f);
        GameObject.Find("default").GetComponent<Renderer>().material.color = normal;
    }

    //Destroy this enemy, create an explosion
    IEnumerator Die()
    {
        //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Destroyed Enemy").SetEventAction("Destroyed Tough Enemy"));
        Vector3 exp = transform.position;
        Instantiate(explosion, exp, Quaternion.Euler(Vector3.zero));
        Destroy(gameObject);
        yield return new WaitForSeconds(0.1f);
    }

    //Play the visual effect when entering the play area
    IEnumerator Distort()
    {
        Vector3 exp = transform.position;
        GameObject field = (GameObject)Instantiate(distortion, exp, Quaternion.Euler(Vector3.zero));
        field.transform.parent = gameObject.transform;
        yield return new WaitForSeconds(0.1f);
    }

    //Lose one point of health when damaged
    void LoseHealth()
    {
        health--;
    }

    //Check to see if an object collides with this enemy
    void OnCollisionEnter(Collision other)
    {
        switch (other.gameObject.tag)
        {
            case "Projectile":
                LoseHealth();               //Lose one point of health
                Destroy(other.gameObject);  //Destroy the projectile to reduce buildup
                StartCoroutine(Damaged());  //Flash red
                break;
        }
    }

    //Check if another object intersects with this enemy
    void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "MainCamera":
                //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Damaged By Enemy").SetEventAction("Damaged By Tough Enemy"));
                player.GetComponent<Player>().LoseHealth(2);//Take 2 health points away from the player
                Destroy(gameObject);                        //Destroy this enemy
                break;
            case "Effect":
                warp.Play();                //Play the sound effect
                StartCoroutine(Distort());  //Play the visual effect
                break;
        }
    }
}