﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    private Rigidbody rb;   //The rigidbody for collision detection
    private float lifetime; //The lifetime of the bullet to reduce buildup

	// Use this for initialization
	void Start () {
        GameObject camera = GameObject.Find("Main Camera"); //Locate the camera (player) object
        rb = GetComponent<Rigidbody>();                     //Get the rigidbody of the bullet
        transform.Rotate(camera.transform.forward);         //Point bullet in direction of the camera
        rb.AddForce(camera.transform.forward * 2000);       //Apply force to the bullet to launch it
	}
	
	// Update is called once per frame
	void Update () {
        lifetime += 1f * Time.deltaTime;    //Store how long the bullet hads been live
        if(lifetime > 3)
        {
            Destroy(gameObject);            //Destroy the bullet if it has been alive longer than three seconds
        }
	}

    //Check if a collider is leaving the effect field
    void OnTriggerExit(Collider other)
    {
        switch (other.tag)//Destroy the bullet if it leaves the play area
        {
            case "Effect":
                Destroy(gameObject);
                break;
        }
    }
}
