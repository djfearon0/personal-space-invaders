﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PersonalSpaceInvaders : MonoBehaviour {

    //public GoogleAnalyticsV4 googleAnalytics;//Google Analytics Plugin for Unity

    public AudioSource end;         //The game over sound
    public GameObject commonEnemy;  //The common enemy prefab
    public GameObject toughEnemy;   //The tough enemy prefab
    public Font f;                  //The borg9 font
    public RawImage crosshair;      //The crosshair image for the middle of the screen
    public Text healthLabel;        //Reference to the text that says "Health" on screen
    public Text healthPoints;       //Reference to the empty text object to show current player health
    public Text scoreLabel;         //Reference to the text that says "Score" on screen
    public Text scorePoints;        //Reference to the empty text object to show the current player score

    public bool gameover = false;   //Boolean value for alerting that the game is over
    public bool paused = false;     //Boolean value for toggling the pause menu
    private float[] modifiers;      //Value modifiers for enemy spawning logic
    private int score = 0;          //The player score, starts at zero

	// Use this for initialization
	void Start () {

        //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Number of Games Played").SetEventAction("Started a New Round"));

        //Call the enemy spawn method after the first 5 seconds of the game, continue to do so every 8 seconds after
        InvokeRepeating("Spawn", 5f, 8f);

        //Fill the array with ones and negative ones to modify location variables in spawn
        modifiers = new float[4];
        modifiers[0] = -1;
        modifiers[1] = 1;
        modifiers[2] = -1;
        modifiers[3] = 1;

        //Set the score text object on screen to the starting score
        scorePoints.text = "" + score;
    }
	
	// Update is called once per frame
	void Update () {
        //Check if the player paused the game
        if (paused)
        {
            //Stop all movement and activity based on the Time class
            Time.timeScale = 0;
        }
        else
        {
            //Continue all movement and activity
            Time.timeScale = 1;
        }
	}

    //Increase player score
    public void GainPoints(int points)
    {
        score += points;                //Increase player score by the number of points received
        scorePoints.text = "" + score;  //Update the score text object
    }

    //Set the end screen when the game is over
    public void GameOver()
    {
        //googleAnalytics.LogTiming(new TimingHitBuilder().SetTimingCategory("Play Time").SetTimingInterval((long)Time.timeSinceLevelLoad));
        //Pause all movement
        paused = true;
        //Tell the application the game is over
        gameover = true;

        //Play the game over sound effect
        end.Play();

        //Hide the four text objects on screen
        scoreLabel.text = "";
        scorePoints.text = "";
        healthLabel.text = "";
        healthPoints.text = "";
    }

    //Logic to show the new highscore
    void HighscoreBanner()
    {
        GUI.color = new Color(1, 0.95f, 0);                                                             //Set color to gold yellow
        GUI.skin.label.fontSize = 60;                                                                   //Set font size to 60
        GUI.Label(new Rect(Screen.width / 2 - 350, 200, 700, 75), "New Highscore!");                    //Write "New Highscore!"
        GUI.color = Color.white;                                                                        //Return color to white
        GUI.skin.label.fontSize = 40;                                                                   //Set font size to 40
        PlayerPrefs.SetInt("highscore", score);                                                         //Display the player's score as the highscore
        GUI.Label(new Rect(Screen.width / 2 - 50, 300, 400, 50), "" + PlayerPrefs.GetInt("highscore")); //Save the new highscore
    }

    //Put different UI elements on screen
    void OnGUI ()
    {
        if (paused && !gameover)//Check if the player paused the game
        {
            PauseScreen();//Call the pause function
        } else
        {
            crosshair.enabled = true;//Show the crosshair when playing
        }

        if (gameover)//Check if the game is over
        {
            crosshair.enabled = false;                                                  //Hide the crosshair
            GUI.skin.font = f;                                                          //Change the font to borg9
            GUI.skin.label.fontSize = 50;                                               //Change the font size to 50
            GUI.color = new Color(1, 0, 0);                                             //Set the color to red
            GUI.Label(new Rect(Screen.width / 2 - 200, 0, 400, 50), "Game Over");       //Write "Game Over", top center of the screen, red, 50 point font
            GUI.color = Color.white;                                                    //Set the color back to white
            GUI.skin.label.fontSize = 25;                                               //Change the font size to 25
            GUI.Label(new Rect(Screen.width / 2 - 200, 100, 400, 50), "Final Score:");  //Write "Final Score:"
            GUI.Label(new Rect(Screen.width / 2 - 200, 150, 400, 50), "" + score);      //Show the player's final score

            //Check to see if a previous highscore has been saved, act accordingly
            if (PlayerPrefs.HasKey("highscore"))//If a highscore exists
            {
                if(score >= PlayerPrefs.GetInt("highscore")){   //Check if the player's score is higher or the same as the highscore
                    HighscoreBanner();                          //Show the highscore logic
                } else//Player did not beat the current highscore
                {
                    GUI.skin.label.fontSize = 25;                                                                   //Change the font size to 25
                    GUI.Label(new Rect(Screen.width / 2 - 200, 200, 400, 50), "Current Highscore:");                //Write "Current Highscore:"
                    GUI.Label(new Rect(Screen.width / 2 - 200, 250, 400, 50), "" + PlayerPrefs.GetInt("highscore"));//Show the current highscore
                }
            } else//No highscore has been logged yet
            {
                HighscoreBanner();//Show the highscore logic
            }
            if(GUI.Button(new Rect(Screen.width / 2 - 100, Screen.width / 2 - 25, 200, 50), "Main Menu"))//Put menu button on screen, check if it is pressed
            {
                paused = false;                     //Unpause the game for the next time
                gameover = false;                   //Set the next game to not be over
                SceneManager.LoadScene("MainMenu"); //Return to the main menu
            }
        }
    }

    //Show the pause screen when called by the player
    void PauseScreen()
    {
        crosshair.enabled = false;                                                  //Hide the crosshair
        GUI.skin.font = f;                                                          //Change the font to borg9
        GUI.skin.label.fontSize = 60;                                               //Set the font size to 60
        GUI.Label(new Rect(Screen.width / 2 - 175, 100, 350, 70), "Paused");        //Display "Paused" in the center of the screen
        if (GUI.Button(new Rect(Screen.width / 2 - 100, 300, 200, 50), "End Game")) //Put end button, check if it is pressed
        {
            //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Number of Games Ended").SetEventAction("Quit the Game"));
            GameOver();//The button was pressed, immediately go to gameover screen
        }
    }

    //The spawn logic for creating enemies in the game
    void Spawn () {

        float spawnChoice = Random.value;           //Choose a random value between 0 and 1
        float modX = modifiers[Random.Range(0, 3)]; //Choose an X modifier
        float modZ = modifiers[Random.Range(0, 3)]; //Choose a Z modifier
        float x = Random.Range(0, 15);              //Choose a random X position
        float z = Random.Range(0, 15);              //Choose a random Z position

        Vector3 pos = new Vector3();//Create an empty position

        //If one of the values is less than the other, change the other to 15 to keep the enemy outside of the player area initially
        if(x < z)//If x is less than z,
        {
            z = 15;//Z equals 15
        }
        if (z < x)//If z is less than x,
        {
            x = 15;//X equals 15
        }

        pos = new Vector3(modX * x, 0, modZ * z);//Create the enemy position using the modifiers on X and Z respectively

        //Tough enemy spawn rate: 25%
        if (spawnChoice >= 0.75) {
            Instantiate(toughEnemy, pos, Quaternion.LookRotation(Vector3.zero));//Create a new tough enemy
        }
        //Common enemy spawn rate: 75%
        else {
            Instantiate(commonEnemy, pos, Quaternion.LookRotation(Vector3.zero));//Create a new common enemy
        }
    }

    //Toggle the time scale of the game based on the pause button
    public void TogglePause()
    {
        paused = !paused;//Flip the paused boolean
    }
}
