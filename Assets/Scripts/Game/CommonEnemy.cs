﻿using UnityEngine;
using System.Collections;

public class CommonEnemy : MonoBehaviour {

    //public GoogleAnalyticsV4 googleAnalytics;//Google Analytics Plugin for Unity

    public GameObject distortion;//Effect that plays when enemies enter the play area
    public GameObject explosion;//Effect that plays when enemies are destroyed
    private AudioSource warp;//Sound effect that plays when enemies enter the play area
    private Color flash;//Flash of color that occurs when an enemy is hit
    private Color normal;//The original color of the enemy
    private GameObject player;//Reference to the player object

    private int health = 20;//Number of health points
    private int score = 50;//How many points the player gets for destroying this type of enemy

    // Use this for initialization
    void Start ()
    {
        //Reference the main camera, which is the player
        player = GameObject.Find("Main Camera");
        //Get the sound effect that will play when entering the game area
        warp = GetComponent<AudioSource>();

        //Get the original color of the enemy mesh
        flash = normal = GameObject.Find("default").GetComponent<Renderer>().material.color;
        //Change it to red for the damage flash
        flash = Color.red;
    }
	
	// Update is called once per frame
	void Update () {
        //Move and face the enemy toward the player
        transform.position = Vector3.MoveTowards(transform.position, Vector3.zero, 0.5f * Time.deltaTime);
        transform.LookAt(new Vector3(0, -180, 0));

        //Check how much health this enemy has left. If zero or less, destroy enemy
        if (health <= 0)
        {
            StartCoroutine(Die());//Call the die method
            player.GetComponent<PersonalSpaceInvaders>().GainPoints(score);//Call the method in the game script to give points
        }
    }

    /*Game Functions*/

    //Perform a damage flash
    IEnumerator Damaged()
    {
        GameObject.Find("default").GetComponent<Renderer>().material.color = flash;//Flash red
        yield return new WaitForSeconds(0.2f);//Keep the enemy red for 0.2 seconds
        GameObject.Find("default").GetComponent<Renderer>().material.color = normal;//Return the enemy to the original color
    }

    //Set an explosion animation at the position of the enemy, destroy the enemy object
    IEnumerator Die()
    {
        //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Destroyed Enemy").SetEventAction("Destroyed Common Enemy"));
        Vector3 exp = transform.position;
        Instantiate(explosion, exp, Quaternion.Euler(Vector3.zero));
        Destroy(gameObject);
        yield return new WaitForSeconds(0.1f);
    }

    //Apply the visual effect for entering the play area, have it follow the position of the enemy until it is done
    IEnumerator Distort()
    {
        Vector3 exp = transform.position;
        GameObject field = (GameObject)Instantiate(distortion, exp, Quaternion.Euler(Vector3.zero));
        field.transform.parent = gameObject.transform;
        yield return new WaitForSeconds(0.1f);
    }

    //Lose one health point per hit
    void LoseHealth()
    {
        health -= 1;
    }

    //Check to see if a collider makes contact with this enemy
    void OnCollisionEnter (Collision other) {
        switch (other.gameObject.tag)
        {
            case "Projectile"://If it is a projectile, lose one health point
                LoseHealth();
                Destroy(other.gameObject);//Destroy the projectile to eliminate buildup
                StartCoroutine(Damaged());//Perform a damage flash
                break;
        }
    }

    //Check to see if a collider intersects with the game object
    void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "MainCamera"://If enemy hits the player, call method in Player.cs to take away one of their health points
                //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Damaged By Enemy").SetEventAction("Damaged By Common Enemy"));
                player.GetComponent<Player>().LoseHealth(1);
                Destroy(gameObject);//Destroy this enemy
                break;
            case "Effect"://If they enter the effect field, play the sound and visual effects
                warp.Play();
                StartCoroutine(Distort());
                break;
        }
    }
}
