﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    //public GoogleAnalyticsV4 googleAnalytics;//Google Analytics Plugin for Unity

    public GameObject projectile;           //Bullet prefab
    public UnityEngine.UI.Image damageFlash;//Flash of color when player takes damage
    public UnityEngine.UI.Text healthPoints;//Text object for number of health points
    private AudioSource fire;               //Sound when shooting
    private Gyroscope gyro;                 //Device gyroscope
    private PersonalSpaceInvaders game;     //Reference to the game logic
    private Vector3 direction;              //The direction the player is looking

    private bool gameover = false;          //Check if the game is over
    private bool leftFirePressed = false;   //Check if the player pressed the left fire button
    private bool rightFirePressed = false;  //Check if the player pressed the right fire button
    private bool shooting = false;          //Check if the player is shooting
    private int health = 10;                //The health of the player, starts at 10

	// Use this for initialization
	void Start () {
        game = GetComponent<PersonalSpaceInvaders>();                                           //Get the game logic
        gyro = Input.gyro;                                                                      //Access the gyroscope
        gyro.enabled = true;                                                                    //Enable gyroscope usage
        fire = GameObject.Find("Main Camera").GetComponent<AudioSource>();                      //Get the firing sound effect
        healthPoints.text = "" + health;                                                        //Set the health on screen
        damageFlash.transform.localScale = new Vector3(Screen.width / 2, Screen.height / 2, 0); //Expand the color flash to the size of the viewport
        damageFlash.enabled = false;                                                            //Hide the color flash
    }
	
	// Update is called once per frame
	void Update () {
        if (game.paused == false)//Let the player look around while not paused
        {
            direction = new Vector3(-gyro.rotationRateUnbiased.x, -gyro.rotationRateUnbiased.y, gyro.rotationRateUnbiased.z);//Create a new direction vector based on the gyroscope orientation
            transform.Rotate(direction);//Rotate the camera to the new direction
        }

        if (health <= 0)//Check if the player's health is zero or less
        {
            //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Number of Games Ended").SetEventAction("Lost all Health"));
            game.GameOver();//The game is over
            gameover = true;//Set gameover to be true
        }

        //Check if the player presses either, or both, of the fire buttons. Also check if the player is already shooting or the game is over
        if ((leftFirePressed == true || rightFirePressed == true) && shooting == false && gameover == false)
        {
            StartCoroutine(Fire());//Start shooting
        } else
        {
            StopCoroutine(Fire());//Stop shooting
        }
    }

    //CoRoutine for firing projectiles at enemies
    IEnumerator Fire()
    {
        Instantiate(projectile, Vector3.zero, transform.rotation);  //Create a new bullet prefab
        fire.Play();                                                //Play the firing sound
        shooting = true;                                            //Say the player is shooting
        yield return new WaitForSeconds(0.2f);                      //Wait 0.2 seconds between each bullet
        shooting = false;                                           //Set shooting to false
    }

    //Reduce player life
    public void LoseHealth(int lost)
    {
        if (health > 0)//Only reduce if the health is greater than 0
        {
            health -= lost;                 //Reduce player health by 1
            StartCoroutine(ScreenFlash());  //Flash red on the screen to show damage
            healthPoints.text = "" + health;//Show the current health on screen
        }
    }

    //Realign the camera with the level in case gyroscope is out of sync
    public void Realign () {
        //googleAnalytics.LogEvent(new EventHitBuilder().SetEventCategory("Realign").SetEventAction("Realigned The Viewport"));
        Vector3 curr = transform.rotation.eulerAngles;  //Get the current rotation of the player
        curr.x = 0;                                     //Set the x value to 0
        curr.z = 0;                                     //Set the z value to 0
        transform.rotation = Quaternion.Euler(curr);    //Set the rotation level with the play area
    }

    //Flash red on the screen when the player is hit
    IEnumerator ScreenFlash ()
    {
        damageFlash.enabled = true;             //Enable the flash texture
        yield return new WaitForSeconds(0.2f);  //Keep the screen red for 0.2 seconds
        damageFlash.enabled = false;            //Disable the flash texture
    }

    /*Button Logic Start */

    //Left Fire Button Start
    public void LeftFireDown()
    {
        leftFirePressed = true;
    }

    public void LeftFireUp()
    {
        leftFirePressed = false;
    }
    //Left Fire Button End


    //Right Fire Button Start
    public void RightFireDown()
    {
        rightFirePressed = true;
    }

    public void RightFireUp()
    {
        rightFirePressed = false;
    }
    //Right Fire Button End

    /*Button Logic End*/
}
